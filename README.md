# Technical Challenge for Getir
This repository contains the source code of a technical Challenge for Getir Company
## Dependencies used on project
* body-parser
* cors
* dotenv
* express
* express-validator
* helmet
* mangodb
* mongoose
* morgan
* swagger
* winston
* jest
* supertest
* husky
* tslint

## Live Url
```
http://54.227.145.96/api/v0/record
```
## Swagger Doc
```
http://54.227.145.96/api/v0/docs/
```

## Installation witout docker
```
git clone https://bitbucket.org/kharratahmed/getirchallenge.git
cd getirchallenge
npm i
```
### run project Development (with nodemon)
NB: Rename the .env.exemple inside .env folder to .env.dev and fill up values for the env\
ex: APP_PORT=8080 \
DB_CONNECTION_STRING=mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true 
```
npm start
```

### run project Production (with pm2)
NB: Rename the .env.exemple inside the .env folder to .env.prod and fill up values for the env\
ex: APP_PORT=8080 \
DB_CONNECTION_STRING=mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study?retryWrites=true 
```
npm run start:prod
```
## Installation with docker (with pm2 and work only for production)
NB: you must have .env.prod filled up with prod env
```
npm run start:prod:docker
```

## Running Tests (jest && supertest)
```
npm run test
```
NB: 
* Test run automatically before push
* Lint run automatically before commit
* All logs are stored inside /logs folder



