import { check } from 'express-validator';

export const recordValidation = {
    findWithFilter: [
        check('startDate').exists().withMessage('startDate required')
            .isDate({ format: 'YYYY-MM-DD' }).withMessage('invalid startDate value'),

        check('endDate').exists().withMessage('endDate required').
            isDate({ format: 'YYYY-MM-DD' }).withMessage('invalid endDate value')
            .custom((value, { req }) => value > req.body.startDate).withMessage('endDate must be >= the startDate'),

        check('minCount').exists().withMessage('minCount required')
            .isInt({ min: 0 }).withMessage('invalid minCount'),

        check('maxCount').exists().withMessage('maxCount required')
            .isInt().withMessage('invalid maxCount').custom((value, { req }) => value >= req.body.minCount).withMessage('maxCount must be >= minCount')
    ],
};