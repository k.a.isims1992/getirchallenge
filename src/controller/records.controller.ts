import { validationResult } from 'express-validator';
import { errorResponse, validationErrorResponse, successResponse } from '../utils/response.utils';
import records from '../models/record.model';
import { winston } from '../config/winston.config';

export const recordController = {
    getFiltredRecords: (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty())
            return validationErrorResponse(res, errors.array()[0].msg);
        else {
            const { startDate, endDate, minCount, maxCount } = req.body;
            records
                .aggregate([
                    {
                        $match: {
                            createdAt: {
                                $gte: new Date(startDate),
                                $lte: new Date(endDate),
                            },
                        },
                    },
                    {
                        $project: {
                            _id: 0,
                            key: 1,
                            createdAt: 1,
                            totalCount: {
                                $sum: "$counts",
                            },
                        },
                    },
                    {
                        $match: {
                            totalCount: {
                                $gt: Number(minCount),
                                $lt: Number(maxCount),
                            },
                        },
                    },
                ])
                .exec((err, collection) => {
                    if (err) {
                        winston.error(err);
                        return errorResponse(res);
                    }
                    return successResponse(res, collection)
                });
        }
    }
}