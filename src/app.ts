import * as dotenv from "dotenv";
// import env file (dev/prod)
dotenv.config({ path: `${__dirname}/../.env/.env.${process.env.NODE_ENV}` });

import * as express from 'express';
import appRoutes from './routes';

const app = express();
appRoutes(app);

export default app;