import { winston } from "../config/winston.config";
import { statusCode } from "./statusCode.utils";

export const globalError = (err, req, res, next) => {
    winston.error(err);
    return errorResponse(res);
}

export const successResponse = (res, records) => (
    res.status(statusCode.SuccessOK).json({
        code: 0,
        msg: "Success",
        records
    })
);

export const errorResponse = (res) => (
    res.status(statusCode.ServerErrorInternal).json({
        code: 999,
        msg: 'Internal Server Error'
    })
);


export const validationErrorResponse = (res, error) => (
    res.status(statusCode.ClientErrorBadRequest).json({
        code: -1,
        msg: error
    })
);

export const notFoundErrorResponse = (res) => (
    res.status(statusCode.ServerNotFound).json({
        code: -2,
        msg: 'Not Found'
    })
);