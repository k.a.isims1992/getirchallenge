export enum statusCode {
	SuccessOK = 200,
	ClientErrorBadRequest = 422,
	ServerNotFound = 404,
	ServerErrorInternal = 500,
}