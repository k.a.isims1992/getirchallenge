import { Router } from 'express';
import { recordController } from '../controller/records.controller';
import { globalError } from '../utils/response.utils';
import { recordValidation } from '../validation/records.validation';

const recordRoute = Router();

recordRoute.post('', recordValidation.findWithFilter, recordController.getFiltredRecords, globalError);

export default recordRoute;
