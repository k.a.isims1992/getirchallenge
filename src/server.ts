import app from './app';
import * as morgan from 'morgan';
import * as  helmet from 'helmet'
import { mongooseConnect } from './config/db.config';
import { winston, streamWriterMorgan } from "./config/winston.config";

mongooseConnect
  .then(() => {
    app.use(helmet());
    app.use(morgan('combined', { "stream": streamWriterMorgan }));
    app.listen(process.env.APP_PORT, () => {
      winston.info(`App is listening on port ${process.env.APP_PORT}`);
    });
  })
  .catch((e) => {
    winston.error(e, () => {
      process.exit(1);
    });
  });

