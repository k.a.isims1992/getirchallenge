import app from '../../src/app';
import * as request from 'supertest';
import { statusCode } from '../../src/utils/statusCode.utils';
import * as mongoose from 'mongoose';

beforeAll(async () => {
    await mongoose.connect(process.env.DB_CONNECTION_STRING, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false,
    });
});

afterAll(() => {
    mongoose.disconnect()
});

describe('test record data', () => {
    it('POST /api/v0/record Should respond with 200', async () => {
        const res = await request(app).post('/api/v0/record')
            .send({
                startDate: "2016-01-26",
                endDate: "2018-02-02",
                minCount: 2700,
                maxCount: 2710
            });
        expect(res.status).toBe(statusCode.SuccessOK);
        expect(res.body.code).toBe(0);
        expect(Array.isArray(res.body.records)).toBe(true);
        res.body.records.forEach(item => {
            expect(item).toHaveProperty('key');
            expect(item).toHaveProperty('createdAt');
            expect(item).toHaveProperty('totalCount');
            expect(item.totalCount).toBeGreaterThanOrEqual(2700);
            expect(item.totalCount).toBeLessThanOrEqual(3000);
            expect(new Date(item.createdAt).getTime()).toBeGreaterThanOrEqual(new Date('2016-01-26').getTime());
            expect(new Date(item.createdAt).getTime()).toBeLessThanOrEqual(new Date('2018-02-02').getTime());
        });
    });
})


